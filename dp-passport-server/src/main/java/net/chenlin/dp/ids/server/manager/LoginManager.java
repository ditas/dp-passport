package net.chenlin.dp.ids.server.manager;

import net.chenlin.dp.ids.common.entity.SessionData;
import net.chenlin.dp.ids.common.entity.TicketValidateResultDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 登录manager
 * @author zcl<yczclcn@163.com>
 */
public interface LoginManager {

    /**
     * 登录：web
     * @param response
     * @param sessionData
     * @return 返回sessionIdRedisKey
     */
    String login(HttpServletResponse response, SessionData sessionData);

    /**
     * 登录校验：web
     * @param request
     * @return
     */
    String loginCheck(HttpServletRequest request);

    /**
     * 登出：web
     * @param request
     * @param response
     */
    void logout(HttpServletRequest request, HttpServletResponse response);

    /**
     * 登录：app
     * @param sessionData
     * @return
     */
    Map<String, String> login(SessionData sessionData);

    /**
     * 登录校验：app
     * @param sessionId
     * @return
     */
    SessionData loginCheck(String sessionId);

    /**
     * ticket校验：app
     * @param ticket
     * @return
     */
    TicketValidateResultDTO ticketCheck(String ticket);

    /**
     * 登出：app
     * @param sessionId
     */
    void logout(String sessionId);

}
